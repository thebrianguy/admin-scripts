import-module ActiveDirectory

Function CreateUsersHash
{
Param($Fname,$Lname)

    $users = @{
 
    "DisplayName" = "$Lname, $Fname"
    "givenName" = "$Fname"
    "name" = "$Lname, $Fname"
    "SamAccountName" = "$Fname.$Lname"
    "Surname" = "$Lname"
    "Type" = "user"
    "UserPrincipalName" = "$Fname.$Lname@vanir.com"
    "AccountPassword" = (ConvertTo-SecureString $randpass -AsPlainText -Force)
    Enabled = $true
    PasswordNeverExpires  = $true
 
 
    }
 
New-ADUser @users
 
$fname
$lname
$randpass
 
}



$continue = "Yes"
while ($continue -eq "Yes") {
$fname = Read-Host "What is the user's first name?"
$lname = Read-Host "What is the user's last name?"
$randpass = .\Get-RandomString.ps1 -UpperCase:$True -Numbers:$True -Symbols:$True
$continue = Read-Host "More Users? (Yes) or (No)?"


CreateUsersHash $fname $lname

}



