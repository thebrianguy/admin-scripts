$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://exchangehybrid.vanir.com/PowerShell/ -Authentication Kerberos -Credential $UserCredential
Import-PSSession $Session -AllowClobber

Function Enable-MBX
{
Param($Fname,$Lname)

Enable-RemoteMailbox "$Fname.$Lname@vanir.com" -RemoteRoutingAddress "$fname.$lname@vanircminc.mail.onmicrosoft.com"  
}

$continue = "Yes"
while ($continue -eq "Yes") {
$fname = Read-Host "What is the user's first name?"
$lname = Read-Host "What is the user's last name?"
$continue = Read-Host "More Users? (Yes) or (No)?"

Enable-MBX $fname $lname

}