﻿$UserCredential = Get-Credential

Function Remove-Clutter
{
Param($Fname,$Lname)

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection

Import-PSSession $Session -AllowClobber

Get-MailBox "$Fname.$Lname@vanir.com" | Set-Clutter -Enable $false

}


$continue = "Yes"
while ($continue -eq "Yes") {
$fname = Read-Host "What is the user's first name?"
$lname = Read-Host "What is the user's last name?"
$continue = Read-Host "More Users? (Yes) or (No)?"

Remove-Clutter $fname $lname

}

Remove-PSSession $Session