﻿import-module ActiveDirectory

Function CreateUsers
{
Param($Fname,$Lname)

    $users = @{
 
    "DisplayName" = "$Lname, $Fname"
    "givenName" = "$Fname"
    "name" = "$Lname, $Fname"
    "SamAccountName" = "$Fname.$Lname"
    "Surname" = "$Lname"
    "Type" = "user"
    "UserPrincipalName" = "$Fname.$Lname@vanir.com"
    "AccountPassword" = (ConvertTo-SecureString $randpass -AsPlainText -Force)
    Enabled = $true
    PasswordNeverExpires  = $true
 
 
    }
 
New-ADUser @users
 
$fname
$lname
$randpass

Get-ADUser "$fname.$lname" | Move-ADObject -TargetPath 'OU=NewUsers,OU=User Accounts,DC=Vanir,DC=com'
 
}

$UserCredential = Get-Credential

Function Enable-MBX
{
Param($Fname,$Lname)

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://exchangehybrid.vanir.com/PowerShell/ -Authentication Kerberos -Credential $UserCredential
Import-PSSession $Session -AllowClobber

Enable-RemoteMailbox "$Fname.$Lname@vanir.com" -RemoteRoutingAddress "$fname.$lname@vanircminc.mail.onmicrosoft.com"

Remove-PSSession $Session

}


$continue = "Yes"
while ($continue -eq "Yes") {
$fname = Read-Host "What is the user's first name?"
$lname = Read-Host "What is the user's last name?"
$randpass = .\Get-RandomString.ps1 -UpperCase:$True -Numbers:$True -Symbols:$True
$continue = Read-Host "More Users? (Yes) or (No)?"


CreateUsers $fname $lname
Enable-MBX $fname $lname

}



