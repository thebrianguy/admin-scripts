﻿$UserCredential = Get-Credential

Function Change-ADUser-Address
{
Param($Fname, $Lname)
$name = "$Fname.$Lname"
Get-ADUser $name
if ($? -eq $false)     <# Check if the first.lastname is valid otherwise use firstinitial.lastname #>
    {
    $Fname = $Fname[0]
    $name = "$Fname$Lname"
    Get-ADUser $name
    }

Get-ADUser $name | Set-ADUser -StreetAddress $address -Office $office
}

"Change multiple users to the same new address"
$address = Read-Host "What is the new address? Enter the | (pipe character) for newlines"
$address = $address.replace("|","`r`n")
$office = Read-Host "What is the office name?"

$continue = "Yes"
while ($continue -eq "Yes") {
$fname = Read-Host "What is the user's first name?"
$lname = Read-Host "What is the user's last name?"

Change-ADUser-Address $fname $lname

$continue = Read-Host "More Users? (Yes) or (No)?"
}