This repository will hold various scripts to automate administrative tasks in AD, Exchange and other related subsystems. 

Scripts should be organized into seperate folders based off a task that it accomplishes.

Most will probably start out as simple manually typed commands and should evolve into automatic scripts that may need certain parameters from the user (admin) using the script.