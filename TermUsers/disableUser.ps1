﻿$UserCredential = Get-Credential

Function Move-ADUser
{
Param($Fname, $Lname)
$name = "$Fname.$Lname"
Get-ADUser $name
if ($? -eq $false)     <# Check if the first.lastname is valid otherwise use firstinitial.lastname #>
    {
    $Fname = $Fname[0]
    $name = "$Fname$Lname"
    Get-ADUser $name
    }

Get-ADUser $name | Move-ADObject -TargetPath 'OU=User Accounts Terminated Pending,OU=Termination,DC=Vanir,DC=com'

$randpass = .\Get-RandomString.ps1 -UpperCase:$True -Numbers:$True -Symbols:$True <# Call another script to create a random password #>

Get-ADUser $name | Set-ADAccountPassword -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$randpass" -Force)

Get-ADUser $name | Disable-ADAccount

$description = Read-Host "What description to put on the account?"

Get-ADUser $name | Set-ADUser -Description $description

Get-ADPrincipalGroupMembership -Identity $name | % { if ($_.name -ne "Domain Users") {Remove-ADPrincipalGroupMembership -Identity $name -MemberOf $_ -Confirm:$False}}

}


Function Hide-MBX
{
Param($Fname,$Lname)

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://exchangehybrid.vanir.com/PowerShell/ -Authentication Kerberos -Credential $UserCredential
Import-PSSession $Session -AllowClobber

Set-RemoteMailbox -Identity "$Fname.$Lname@vanir.com" -HiddenFromAddressListsEnabled $true

Remove-PSSession $Session
}

Function Disable-MBX
{
Param($Fname,$Lname)

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection

Import-PSSession $Session -AllowClobber

Set-CASMailbox -Identity "$Fname.$Lname@vanir.com" -MAPIEnabled $false -PopEnabled $false -ImapEnabled $false -OWAEnabled $false -ActiveSyncEnabled $false -OWAforDevicesEnabled $false

get-mobiledevice -mailbox "$Fname.$Lname@vanir.com" | foreach-object {Remove-ActiveSyncDevice ([string]$_.Guid) -confirm:$false}

Remove-PSSession $Session
}

$continue = "Yes"
while ($continue -eq "Yes") {
$fname = Read-Host "What is the user's first name?"
$lname = Read-Host "What is the user's last name?"
$continue = Read-Host "More Users? (Yes) or (No)?"

Move-ADUser $fname $lname
Hide-MBX $fname $lname
Disable-MBX $fname $lname

}