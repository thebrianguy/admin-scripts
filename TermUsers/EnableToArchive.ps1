﻿$UserCredential = Get-Credential

Write-Host "The following is asking for the name of the user who needs access:"
$fnameGrant = Read-Host "What is the user's first name?"
$lnameGrant = Read-Host "What is the user's last name?"

Function Enable-ADUser
{
Param($Fname, $Lname)
$name = "$Fname.$Lname"
Get-ADUser $name
if ($? -eq $false)     <# Check if the first.lastname is valid otherwise use firstinitial.lastname #>
    {
    $Fname = $Fname[0]
    $name = "$Fname$Lname"
    Get-ADUser $name
    }

Get-ADUser $name | Enable-ADAccount
Get-ADUser $name | Move-ADObject -TargetPath 'OU=User Accounts Terminated,OU=Termination,DC=Vanir,DC=com'

}

Function Unhide-MBX
{
Param($Fname,$Lname)

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://exchangehybrid.vanir.com/PowerShell/ -Authentication Kerberos -Credential $UserCredential
Import-PSSession $Session -AllowClobber

Set-RemoteMailbox -Identity "$Fname.$Lname@vanir.com" -HiddenFromAddressListsEnabled $false

Remove-PSSession $Session
}

Function Enable-MBX
{
Param($Fname,$Lname,$FnameGrant,$LnameGrant)

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection

Import-PSSession $Session -AllowClobber

Set-CASMailbox -Identity "$Fname.$Lname@vanir.com" -MAPIEnabled $true

Add-MailboxPermission -Identity "$Fname.$Lname@vanir.com" -User "$FnameGrant.$LnameGrant@vanir.com" -AccessRights FullAccess -InheritanceType All -AutoMapping $false

Remove-PSSession $Session
}


$continue = "Yes"
while ($continue -eq "Yes") {
Write-Host "The following is asking for the name of the disabled user:"
$fname = Read-Host "What is the user's first name?"
$lname = Read-Host "What is the user's last name?"
$continue = Read-Host "More Users? (Yes) or (No)?"

Enable-ADUser $fname $lname
Unhide-MBX $fname $lname
Enable-MBX $fname $lname $fnameGrant $lnameGrant

}